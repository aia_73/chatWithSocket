//
//  ConnectViewController.swift
//  ChatWithSocket
//
//  Created by alex borisoft on 01.02.17.
//  Copyright © 2017 CreActive. All rights reserved.
//

import UIKit
import AVFoundation

class ConnectViewController: BaseViewController, AVAudioPlayerDelegate {
    @IBOutlet weak var hostIp: UITextField!
    @IBOutlet weak var port: UITextField!
     @IBOutlet weak var toChatButton: UIButton!
    @IBOutlet weak var indicatorConnect: UIView!
    var player = AVAudioPlayer()
    var recordingSession: AVAudioSession!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func onClickConnect(_ sender: UIButton) {
        let  manager = ConnectionManager.sharedInstance
//        if manager.connect(ipAddr: hostIp.text!, port: Int(port.text!)!) {
//            
//        }
        self.toChatButton.isUserInteractionEnabled = true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var dataSource: FakeDataSource!
        
        let chatController = { () -> ChatViewController? in
            if let controller = segue.destination as? ChatViewController {
                return controller
            }
            if let tabController = segue.destination as? UITabBarController,
                let controller = tabController.viewControllers?.first as? ChatViewController {
                return controller
            }
            return nil
            }()!
        
        if dataSource == nil {
            dataSource = FakeDataSource.sharedInstance
        }
        chatController.dataSource = dataSource
        chatController.messageSender = dataSource.messageSender
    }

    

}
