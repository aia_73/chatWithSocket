//
//  ConnectivityLayer.swift
//  ChatWithSocket
//
//  Created by alex borisoft on 03.02.17.
//  Copyright © 2017 CreActive. All rights reserved.
//

import UIKit
import SwiftSocket

class ConnectivityLayer: NSObject {
    
    var client: TCPClient? = nil
    var config: ServerConfig? = nil
    
    static let sharedInstance : ConnectivityLayer = {
        let instance = ConnectivityLayer()
        
        return instance
    }()
    
    func setup(config: ServerConfig) {
        self.config = config
    }
    
    func createSession() -> Bool {
        self.client = TCPClient.init(address: self.config!.endpoint, port: self.config!.port)
        let result:Result = self.client!.connect(timeout: self.config!.defaultTimeout)
        
        switch result {
            case .success:
                print("connection")
                return true
        
            case .failure:
                print("bad")
                return false
        }
    }
    
    func send(data:Data) -> Result {
        return (self.client?.send(data: data))!
    }
    
    func read(size:Int) -> [Byte]? {
        return self.client?.read(size)
    }
}
