//
//  ConnectionManagerProtocols.swift
//  ChatWithSocket
//
//  Created by Ivan Tsaryov on 08/02/17.
//  Copyright © 2017 CreActive. All rights reserved.
//

import Foundation

protocol ConnectionManagerCompletion {
    
    func didConnect(manager: ConnectionManager) -> Void
    func didFailConnect(manager: ConnectionManager) -> Void
    
    func didFailRequest(manager: ConnectionManager, error: Error) -> Void
    
    func didGetCommandList(manager: ConnectionManager, commands: String) -> Void
    func didGetFileList(manager: ConnectionManager, files: String) -> Void
    func didGetFileInfo(manager: ConnectionManager, info: String) -> Void
    func didGetFile(manager: ConnectionManager, file: FileManager) -> Void
}
