//
//  ConnectionManager.swift
//  ChatWithSocket
//
//  Created by alex borisoft on 03.02.17.
//  Copyright © 2017 CreActive. All rights reserved.
//

import UIKit

class ConnectionManager: NSObject {
    let completion: ConnectionManagerCompletion? = nil
    let defaultSize:Int = 64 * 1024
    
    let layer = ConnectivityLayer.sharedInstance
        static let sharedInstance : ConnectionManager = {
        let instance = ConnectionManager()
        return instance
    }()
    
    func connect(endpoint: String, port: Int32 ) -> Void {
        let config: ServerConfig = ServerConfig.init(endpoint: endpoint, port: port)
        
        layer.setup(config: config)
        
        let res = layer.createSession()
        
        guard res else {
            self.completion?.didFailConnect(manager: self)
            
            return
        }

        self.completion?.didConnect(manager: self)
    }

    func getComandsList() -> Void {
        let query:String = "getComandsList"
        
        self.execute(query: query, success: { (manager, result) in
            self.completion?.didGetCommandList(manager: manager, commands: result)
        }) { (manager, error) in
            self.completion?.didFailRequest(manager: manager, error: error)
        }
    }
    
    func getFileList() -> Void {
        let query: String = "getFileList"
        
        self.execute(query: query, success: { (manager, result) in
            self.completion?.didGetFileList(manager: manager, files: result)
        }) { (manager, error) in
            self.completion?.didFailRequest(manager: manager, error: error)
        }
    }
    
    func getFileInfo(name: String)-> Void {
        let query: String = "getInfo " + name
        
        self.execute(query: query, success: { (manager, result) in
            self.completion?.didGetFileInfo(manager: manager, info: result)
        }) { (manager, error) in
            self.completion?.didFailRequest(manager: manager, error: error)
        }
    }
    
    //MARK - Private methods
    
    private func execute(query: String,
                         success: @escaping (ConnectionManager, String) -> Void,
                         failure: @escaping (ConnectionManager, Error) -> Void) -> Void {
        DispatchQueue.global(qos: .default).async {
            let res = self.layer.send(data: query.data(using: .utf8)!)
            
            DispatchQueue.main.async {
                if res.isSuccess {
                    let response: [UInt8] = self.layer.read(size: self.defaultSize)!
                    let result: String = String.init(bytes: response, encoding: .utf8)!
                    
                    success(self, result)
                }
                
                failure(self, res.error!)
            }
        }
    }
}
