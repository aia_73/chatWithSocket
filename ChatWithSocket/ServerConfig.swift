//
//  ServerConfig.swift
//  ChatWithSocket
//
//  Created by Ivan Tsaryov on 08/02/17.
//  Copyright © 2017 CreActive. All rights reserved.
//

import Foundation

class ServerConfig {
    var endpoint: String = "192.168.0.105"// true
    var port: Int32 = 3128
    let defaultTimeout: Int = 18
    
    init(endpoint: String, port: Int32) {
        self.endpoint = endpoint
        self.port = port
    }
    
}
